﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	
   public Joystick joystick;
   public CharacterController controller;
	public GameObject coin, compliteGame,looseGame,box, buttonRun;
	private bool compliteGameBool, idle,walk,run;
	public Text timerText, amountCointText,finishText;
	private float timerFloat, amountCointFloat;
	public Animator animController;
	public Image buttonRunImoge;
   private float speed;
   public float gravity;

   
   Vector3 moveDirection;
 
   
      
   
   
   void Start()
   {	speed = 3;
	   compliteGameBool=false;
	   coin = GameObject.FindGameObjectWithTag("coin");
	   for(int i=0; i<=15;i++)
	   {
		   Instantiate(coin,new Vector3 (Random.Range(14f,90f),15,Random.Range(14,87f)),Quaternion.identity);
	   }
	   
	   looseGame.SetActive(false);
	   compliteGame.SetActive(false);
	  
	   timerFloat =60f;
	//    Image=buttonRun.GetComponent<Image>();
   }
   
   void Update()
   {
	   if(run)
	   {
		   speed = 7;
		   buttonRunImoge.color= Color.red;
		// buttonRun.GetComponent<Image>().;
	   }		   
	   else 
	   {
		   speed = 3;
		   buttonRunImoge.color= Color.white;
	   }
	if(timerFloat>0&&compliteGameBool==false)
	{
		timerFloat -=Time.deltaTime;
	   timerText.text = timerFloat.ToString("F1");
	}
	else
	{
		looseGame.SetActive(true);
			timerText.text = " ";
			amountCointText.text= " ";
			buttonRun.SetActive(false);
	}
	   
		

		if(moveDirection.x==0&&moveDirection.z==0)
		{
			idle=true;
			walk = false;
			run = false;
			

			
			
			
		}
		else if(animController.GetBool("runBool")==false)
		{
			idle=false;
			walk = true;
			
		}
		else
		{
			 if(run)
			 {
				 walk = false;

			 }
		}





	   Vector2 direction = joystick.direction;
	   
	   if(controller.isGrounded){
            moveDirection = new Vector3(direction.x, 0, direction.y);
			
			Quaternion targetRotation = moveDirection != Vector3.zero ? Quaternion.LookRotation(moveDirection) : transform.rotation;
			transform.rotation = targetRotation;
	
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
		animController.SetBool("idleBool",idle);
		animController.SetBool("walkBool",walk);
		animController.SetBool("runBool",run);
		
		
		
		
			
   }
   void OnCollisionEnter(Collision other)
   {
	   if(other.gameObject.tag == "coin")
	   {
		   amountCointFloat +=1;
		   amountCointText.text =amountCointFloat.ToString();
		   Destroy(other.gameObject);
	   }

	   if(other.gameObject.tag == "Aim")
	   {
		   
		   compliteGameBool = true;
		   compliteGame.SetActive(true);
		   buttonRun.SetActive(false);
		   timerText.text = " ";
			amountCointText.text= " ";
			finishText.text=amountCointFloat.ToString();
	   }
	   Debug.Log(other.gameObject.name);

   }
   public void ButtonRun()
   {	
	   
	   run = !run;
	   walk = false;
		animController.SetBool("runBool",run);
		if(animController.GetBool("idleBool")==true)
		{
			run = false;
		}


   }
   public void ButtonAgain()
   {
	   SceneManager.LoadScene(0);

   }
   
   
}
