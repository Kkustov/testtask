﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlCam : MonoBehaviour
{
public Transform target;
public float speed=1f;
private Vector3 _position;


    
    void Start()
    {
        
        _position = target.InverseTransformPoint(transform.position);

    }

    // Update is called once per frame
    void Update()
    {
        var currentPosition = target.TransformPoint(_position);
        transform.position = Vector3.Lerp(transform.position,currentPosition, speed*Time.deltaTime);
        transform.LookAt(target); 
        var currentRotation = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, currentRotation, speed*Time.deltaTime);
        
    }
}
